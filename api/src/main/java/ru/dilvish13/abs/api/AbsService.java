package ru.dilvish13.abs.api;

import static com.lightbend.lagom.javadsl.api.Service.named;
import static com.lightbend.lagom.javadsl.api.Service.restCall;

import akka.NotUsed;
import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.api.transport.Method;
import ru.dilvish13.abs.api.common.ResultData;

import java.util.List;
import java.util.Optional;

/**
 * The ABS service interface.
 */
public interface AbsService extends Service {

  ServiceCall<NotUsed, ResultData<AccountsData>> getAccounts(Optional<Integer> pageNum);
  ServiceCall<NotUsed, ResultData<AccountData>> getAccount(String account);
  ServiceCall<NotUsed, ResultData<AccountBalanceData>> getBalance(String id);
  ServiceCall<NotUsed, ResultData<List<PaytureData>>> getPaytures(String id);
  ServiceCall<NotUsed, ResultData<List<PaytureData>>> getAllPaytures();
  ServiceCall<PaytureData, ResultData<?>> doInnerPayture();
  ServiceCall<NotUsed, ResultData<PaytureData>> doFakePayture();

  @Override
  default Descriptor descriptor() {
    // @formatter:off
    return named("abs")
            .withCalls(
                    restCall(Method.GET, "/api/abs/accounts?pageNum", this::getAccounts),
                    restCall(Method.GET, "/api/abs/accounts/:account", this::getAccount),
                    restCall(Method.GET, "/api/abs/accounts/:account/balance", this::getBalance),
                    restCall(Method.GET,"/api/abs/accounts/:account/paytures", this::getPaytures),
                    restCall(Method.GET,"/api/abs/paytures", this::getAllPaytures),
                    restCall(Method.POST,"/api/abs/paytures", this::doInnerPayture),
                    restCall(Method.POST,"/api/abs/paytures-fake", this::doFakePayture)
            )
            .withAutoAcl(true);
    // @formatter:on
  }
}
