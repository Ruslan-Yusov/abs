package ru.dilvish13.abs.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import java.util.List;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;

@Value
@Builder
@JsonDeserialize
@JsonInclude(NON_EMPTY)
@AllArgsConstructor(onConstructor_ = {@JsonCreator})
public class AccountsData {
  // Yes, can use PTreeVector here, but too lazy for these tricks
  List<AccountData> accounts;
  Integer pageNum;
  Integer pageCount;
}
