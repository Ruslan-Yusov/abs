package ru.dilvish13.abs.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;

@Data
@Builder
@JsonDeserialize
@JsonInclude(NON_EMPTY)
@AllArgsConstructor(onConstructor_ = {@JsonCreator})
public class AccountData {
  String number;
  String currency;
  String swift;
}
