package ru.dilvish13.abs.impl

import com.fasterxml.jackson.databind.ObjectMapper
import com.lightbend.lagom.javadsl.testkit.ServiceTest
import com.lightbend.lagom.javadsl.testkit.ServiceTest.TestServer
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import ru.dilvish13.abs.api.AbsService
import ru.dilvish13.abs.api.AccountBalanceData
import ru.dilvish13.abs.api.AccountData
import ru.dilvish13.abs.api.PaytureData
import ru.dilvish13.abs.api.common.GenericStatus
import ru.dilvish13.abs.api.common.ResultData
import java.math.BigDecimal
import java.util.concurrent.TimeUnit

private const val TEST_ACCOUNT_RUB_1 = "30101810600000000786"
private const val TEST_ACCOUNT_RUB_2 = "123458100001"
private const val TEST_ACCOUNT_RUB_3 = "123458100002"
private const val TEST_ACCOUNT_USD_1 = "123458400001"

class AbsServiceKtTest {
    @Test
    fun initialValues() {
        ServiceTest.withServer(ServiceTest.defaultSetup().withCassandra(false).withCluster(false)) { server: TestServer ->
            val service = server.client(AbsService::class.java)

            val expectedRes1 = AccountBalanceData(AccountData(TEST_ACCOUNT_RUB_1, "RUB", "SWIFT000"), BigDecimal.ZERO.setScale(2))
            val res1 = service.getBalance(TEST_ACCOUNT_RUB_1).invoke().toCompletableFuture()[5, TimeUnit.SECONDS]
            assertNotNull(res1.meta)
            assertEquals("OK", res1.meta.status.toString())
            assertNotNull(res1.data)
            assertEquals(expectedRes1, res1.data)
            val expectedRes2 = AccountBalanceData(AccountData(TEST_ACCOUNT_RUB_2, "RUB", "SWIFT000"), BigDecimal(95).setScale(2))
            val res2 = service.getBalance(TEST_ACCOUNT_RUB_2).invoke().toCompletableFuture()[5, TimeUnit.SECONDS]
            assertEquals(expectedRes2, res2!!.data)

            val expectedRes1u = AccountBalanceData(AccountData(TEST_ACCOUNT_USD_1, "USD", "SWIFT000"), BigDecimal(200).setScale(2))
            val res1u =
                service.getBalance(TEST_ACCOUNT_USD_1).invoke().toCompletableFuture()[5, TimeUnit.SECONDS]
            assertEquals(expectedRes1u, res1u!!.data)
        }
    }

    @Test
    fun payturesInvalid() {
        ServiceTest.withServer(ServiceTest.defaultSetup().withCassandra(false).withCluster(false)) { server: TestServer ->
            val service = server.client(AbsService::class.java)

            val expectedRes0 = ResultData(
                meta = ResultData.Meta(GenericStatus("OK")),
                data = AccountBalanceData(
                    AccountData(TEST_ACCOUNT_RUB_2, "RUB", "SWIFT000"),
                    BigDecimal(95).setScale(2)
                )
            )
            val res0 = service.getBalance(TEST_ACCOUNT_RUB_2).invoke().toCompletableFuture()[5, TimeUnit.SECONDS]
            assertNotNull(res0.meta)
            assertEquals("OK", res0.meta.status.toString())
            assertEquals(expectedRes0, res0)

            val payture1 = PaytureData(
                0,
                AccountData(TEST_ACCOUNT_RUB_2, null, null),
                AccountData(TEST_ACCOUNT_RUB_3, null, null),
                BigDecimal(56),
                "RUR", null, null, null)
            val expectedRes1 = ResultData<Any>(
                meta = ResultData.Meta(
                    GenericStatus("ERROR"),
                    "error.transaction.check",
                    "No money for transaction."
                )
            )
            val res1 = service.doInnerPayture().invoke(payture1).toCompletableFuture()[5, TimeUnit.SECONDS]
            assertNotNull(res1.meta)
            assertNull(res1.data)
            assertEquals(expectedRes1, res1)

        }
    }

    @Test
    fun payturesRub() {
        ServiceTest.withServer(ServiceTest.defaultSetup().withCassandra(false).withCluster(false)) { server: TestServer ->
            val service = server.client(AbsService::class.java)

            val payture2 = PaytureData(
                0,
                AccountData(TEST_ACCOUNT_RUB_2, null, null),
                AccountData(TEST_ACCOUNT_RUB_3, null, null),
                BigDecimal(6),
                "RUR", null, null, null)
            val res2 = service.doInnerPayture().invoke(payture2).toCompletableFuture()[5, TimeUnit.SECONDS]
            assertNotNull(res2.meta)
            assertEquals("OK", res2.meta.status.toString())
            assertNull(res2.data)

            val res3 = service.getBalance(TEST_ACCOUNT_RUB_2).invoke().toCompletableFuture()[5, TimeUnit.SECONDS]
            assertNotNull(res3.meta)
            assertEquals("OK", res3.meta.status.toString())
            assertEquals(101, res3!!.data!!.amount!!.toInt())

        }
    }

    @Test
    fun payturesUsd() {
        ServiceTest.withServer(ServiceTest.defaultSetup().withCassandra(false).withCluster(false)) { server: TestServer ->
            val service = server.client(AbsService::class.java)

            val expectedRes4 = "{\"account\":{\"number\":\"$TEST_ACCOUNT_USD_1\",\"currency\":\"USD\",\"swift\":\"SWIFT000\"},\"amount\":200.00}"
                .fromJson(AccountBalanceData::class.java)
            val res4 = service.getBalance(expectedRes4.account.number).invoke().toCompletableFuture()[5, TimeUnit.SECONDS]
            assertNotNull(res4.meta)
            assertEquals(expectedRes4, res4!!.data)
        }
    }

    @Test
    fun initialValuesJ() {
        AbsServiceJTest.doInitialValues()
    }

    private fun <T> String.fromJson(clazz: Class<T>) = this.let {ObjectMapper().readerFor(clazz).readValue<T>(it) }
}